/**
 * @file
 * Send own content height to the parent window.
 *
 * To be included on HTML pages intended to be embedded in an iframe.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.scrapableInitIframeSize = {
    attach: function (context) {
      var settings = drupalSettings.scrapable;

      var embedOriginServer = null;
      var embedReferrer = null;

      // Pull the referrer in as the origin URL, either from the query string
      // or from the document.referrer.
      var matches = window.location.search.match(/referrer=([^&]+)/);
      if (matches && matches[1]) {
        embedReferrer = decodeURIComponent(matches[1]);
      }
      else {
        embedReferrer = document.referrer;
      }

      // Strip any hash tags, since we use them for communication.
      embedReferrer = embedReferrer.replace(/#.*/, '');

      // Set the origin server based on referrer if needed.
      matches = embedReferrer.match(/http[s]?:\/\/[^\/]+/);
      if (matches) {
        embedOriginServer = matches[0];
      }

      function sendSizeResponse() {

        // Safety check that we have the necessary information for sending messages.
        if (!embedOriginServer || !embedReferrer) {
          return;
        }

        var height = $(settings.wrapper_selector).height();

        // HTML5 messaging support approach.
        if (window.parent && window.parent.postMessage) {
          var response = {
            type: 'resize',
            contentHeight: height
          };
          window.parent.postMessage(JSON.stringify(response), embedOriginServer);
        }
        // Set the anchor on the parent frame, which will be read by the parent JS.
        else {
          parent.location = embedReferrer + '#_h=' + height;
        }
      }

      if (window.postMessage) {
        window.addEventListener("message", function(evt) {
          var msg;
          try {
            msg = JSON.parse(evt.data);
          }
          catch(err) {
            // No valid JSON message.
            return;
          }
          if (msg.type === 'resize') {
            sendSizeResponse();
          }
        }, false);
      }

      // On click, update the parent frame size, ensuring that conditionals
      // resize the when needed.
      $(document).click(sendSizeResponse);

      // Send the initial size to the parent when loaded.
      $(document).ready(sendSizeResponse);
    }
  };

})(jQuery, Drupal, drupalSettings);
