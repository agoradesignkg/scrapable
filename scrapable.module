<?php

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_theme_suggestions_HOOK() for page theme.
 */
function scrapable_theme_suggestions_page(array $variables) {
  $theme_hook_suggestions = [];
  if (scrapable_check_route_is_embedded(\Drupal::routeMatch())) {
    $original = $variables['theme_hook_original'];
    $theme_hook_suggestions[] = $original . '__' . 'embedded';
  }
  return $theme_hook_suggestions;
}

/**
 * Implements hook_theme_by_author_route_author().
 */
function scrapable_theme_by_author_route_author(RouteMatchInterface $route_match) {
  $route_name = $route_match->getRouteName();
  switch ($route_name) {
    case 'scrapable.node':
      /** @var \Drupal\node\NodeInterface $node */
      $node = $route_match->getParameter('node');
      return $node->getOwner();
      break;
  }
}

/**
 * Implements hook_preprocess_html().
 */
function scrapable_preprocess_html(&$variables) {
  if (scrapable_check_route_is_embedded(\Drupal::routeMatch())) {
    $variables['attributes']['class'][] = 'embedded-page';
  }
}

/**
 * Implements hook_metatag_route_entity().
 */
function scrapable_metatag_route_entity(RouteMatchInterface $route_match) {
  $route_name = $route_match->getRouteName();
  if ($route_name == 'scrapable.node') {
    if ($node = $route_match->getParameter('node')) {
      return $node;
    }
  }
  return NULL;
}

/**
 * Checks whether the given route match is an embedded route.
 *
 * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
 *   The route match object.
 *
 * @return boolean
 *   TRUE, if the given route match is an embedded route, FALSE otherwise.
 */
function scrapable_check_route_is_embedded(RouteMatchInterface $route_match) {
  $embedded_routes = ['scrapable.webform'];
  $route_name = $route_match->getRouteName();
  return !empty($route_name) && in_array($route_name, $embedded_routes);
}
