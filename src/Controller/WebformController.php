<?php

namespace Drupal\scrapable\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\MainContent\MainContentRendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\scrapable\ScrapableContent;
use Drupal\user\Entity\User;
use Drupal\webform\WebformInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for serving webforms as scrapable content.
 */
class WebformController extends ControllerBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * The HTML renderer service.
   *
   * @var \Drupal\Core\Render\MainContent\MainContentRendererInterface
   */
  protected $htmlRenderer;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Creates an WebformController object.
   *
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher.
   * @param \Drupal\Core\Render\MainContent\MainContentRendererInterface $html_renderer
   *   The HTML renderer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(AccountSwitcherInterface $account_switcher, MainContentRendererInterface $html_renderer, RequestStack $request_stack, RouteMatchInterface $route_match) {
    $this->accountSwitcher = $account_switcher;
    $this->htmlRenderer = $html_renderer;
    $this->requestStack = $request_stack;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('account_switcher'),
      $container->get('main_content_renderer.html'),
      $container->get('request_stack'),
      $container->get('current_route_match')
    );
  }

  /**
   * Renders a webform in an embedable manner, without any other blocks, etc.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   *
   * @return \Drupal\scrapable\ScrapableContent
   *   A ScrapableContent object.
   */
  public function view(WebformInterface $webform) {
    $this->accountSwitcher->switchTo(User::getAnonymousUser());
    $build = $webform->getSubmissionForm();
    $this->accountSwitcher->switchBack();

    $js_settings = [
      'wrapper_selector' => 'body',
    ];
    // @todo we need an alter hook here to be able to change the settings!
    $build['#attached']['drupalSettings']['scrapable'] = $js_settings;
    $build['#attached']['library'][] = 'scrapable/iframe.size';

    return new ScrapableContent($build, TRUE);
  }

  /**
   * Page title callback for a webform.
   *
   * @param \Drupal\webform\WebformInterface $webform
   *   The webform.
   *
   * @return string
   *   The page title.
   */
  public function pageTitle(WebformInterface $webform) {
    return $webform->label();
  }

}
