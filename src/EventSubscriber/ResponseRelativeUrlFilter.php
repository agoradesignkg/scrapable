<?php

namespace Drupal\scrapable\EventSubscriber;

use Drupal\scrapable\HtmlUtil;
use Drupal\scrapable\Render\ScrapableHtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that transforms root relative urls to absolute ones.
 *
 * Event subscriber that transforms root relative urls to absolute ones, in
 * case of having a ScrapableHtmlResponse as the current route's response.
 */
class ResponseRelativeUrlFilter implements EventSubscriberInterface {

  /**
   * Converts relative URLs to absolute URLs.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response event.
   */
  public function onResponse(FilterResponseEvent $event) {
    $response = $event->getResponse();
    // Only care about ScrapableHtmlResponse objects.
    if (!$response instanceof ScrapableHtmlResponse || stripos($event->getResponse()->headers->get('Content-Type'), 'text/html') === FALSE) {
      return;
    }
    $content = $response->getContent();
    $transformed_content = HtmlUtil::transformRootRelativeUrlsToAbsolute($content, $event->getRequest()
      ->getSchemeAndHttpHost());
    $response->setContent($transformed_content);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Should run after any other response subscriber that modifies the markup.
    // @see \Drupal\Core\EventSubscriber\ActiveLinkResponseFilter
    $events[KernelEvents::RESPONSE][] = ['onResponse', -512];

    return $events;
  }

}
