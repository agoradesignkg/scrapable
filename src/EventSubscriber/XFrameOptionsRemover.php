<?php

namespace Drupal\scrapable\EventSubscriber;

use Drupal\scrapable\Render\ScrapableHtmlResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that removes the X-Frame-Options header from scrapable URLs.
 */
class XFrameOptionsRemover implements EventSubscriberInterface {

  /**
   * Removes the X-Frame-Options header from scrapable URLs
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The response event.
   */
  public function setHeaderContentSecurityPolicy(FilterResponseEvent $event) {
    $response = $event->getResponse();
    // Only care about ScrapableHtmlResponse objects.
    if (!$response instanceof ScrapableHtmlResponse || stripos($event->getResponse()->headers->get('Content-Type'), 'text/html') === FALSE) {
      return;
    }
    $response->headers->remove('X-Frame-Options');
    // @todo Should we explicitely list all allowed frame ancestors here? If, should be part of the ScrapableHtmlResponse object.
    //$response->headers->set('Content-Security-Policy', "frame-ancestors 'self' example.com *.example.com", FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['setHeaderContentSecurityPolicy', -10];

    return $events;
  }

}
