<?php

namespace Drupal\scrapable\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\scrapable\Render\MainContent\ScrapableHtmlRenderer;
use Drupal\scrapable\ScrapableContent;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that does correct response handling for scrapable content.
 */
class MainContentViewSubscriber implements EventSubscriberInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * State variable. Holds information, if account is currently switched.
   *
   * @var bool
   */
  protected $accountSwitched;

  /**
   * The scrapable HTML renderer.
   *
   * @var \Drupal\scrapable\Render\MainContent\ScrapableHtmlRenderer
   */
  protected $renderer;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * URL query attribute to indicate the wrapper used to render a request.
   *
   * The wrapper format determines how the HTML is wrapped, for example in a
   * modal dialog.
   */
  const WRAPPER_FORMAT = '_wrapper_format';

  /**
   * Constructs a new MainContentViewSubscriber object.
   *
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher.
   * @param \Drupal\scrapable\Render\MainContent\ScrapableHtmlRenderer $renderer
   *   The scrapable HTML renderer.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(AccountSwitcherInterface $account_switcher, ScrapableHtmlRenderer $renderer, RouteMatchInterface $route_match) {
    $this->accountSwitcher = $account_switcher;
    $this->accountSwitched = FALSE;
    $this->renderer = $renderer;
    $this->routeMatch = $route_match;
  }

  /**
   * Sets a response given a \Drupal\scrapable\ScrapableContent object.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent $event
   *   The event to process.
   */
  public function onViewRenderScrapableContent(GetResponseForControllerResultEvent $event) {
    $request = $event->getRequest();
    $result = $event->getControllerResult();

    if ($result instanceof ScrapableContent) {
      $this->accountSwitcher->switchTo(User::getAnonymousUser());
      $this->accountSwitched = TRUE;
      $this->renderer->setEmbedded($result->isEmbedded());
      $response = $this->renderer->renderResponse($result->getContent(), $request, $this->routeMatch);
      $main_content_view_subscriber_cacheability = (new CacheableMetadata())->setCacheContexts(['url.query_args:' . static::WRAPPER_FORMAT]);
      $response->addCacheableDependency($main_content_view_subscriber_cacheability);
      $event->setResponse($response);
    }
  }

  /**
   * Switches the user back, if necessary.
   *
   * @param \Symfony\Component\HttpKernel\Event\FinishRequestEvent $event
   *   The event to process.
   */
  public function onFinishRequestSwitchBackUser(FinishRequestEvent $event) {
    try {
      if ($this->accountSwitched) {
        $this->accountSwitcher->switchBack();
      }
    }
    catch (\RuntimeException $ex) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::VIEW][] = ['onViewRenderScrapableContent'];
    $events[KernelEvents::FINISH_REQUEST][] = ['onFinishRequestSwitchBackUser'];

    return $events;
  }

}
