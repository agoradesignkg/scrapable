<?php

namespace Drupal\scrapable;

/**
 * Value object representing scrapable content.
 *
 * Used as response value by our controller, for later processing by our event
 * subscribers.
 */
final class ScrapableContent {

  /**
   * The content (a renderable array).
   *
   * @var array
   */
  protected $content;

  /**
   * Whether the response should be rendered embedded (without other regions).
   *
   * @var bool
   */
  protected $embedded;

  /**
   * Creates an ScrapableContent object.
   *
   * @param array $content
   *   The render array as built by the controller.
   * @param bool $embedded
   *   TRUE, if the response should be rendered in embedded mode, FALSE
   *   otherwise.
   */
  public function __construct(array $content, $embedded = FALSE) {
    $this->content = $content;
    $this->embedded = $embedded;
  }

  /**
   * Returns the content.
   *
   * @return array
   *   The content.
   */
  public function getContent() {
    return $this->content;
  }

  /**
   * Returns whether the response should be rendered embedded.
   *
   * @return bool
   *   TRUE, if the response should be rendered in embedded mode, FALSE
   *   otherwise. Embedded mode means only rendering the main region, no other.
   */
  public function isEmbedded() {
    return $this->embedded;
  }

}
