<?php

namespace Drupal\scrapable;

use Drupal\Component\Utility\Html;

/**
 * Html utility class for scrapable content rendering.
 *
 * Utility class for providing helper functions needed for providing the content
 * scrapable, e.g. converting all root-relative URLs to absolute URLs.
 * It is based on the logic of existing similar core functions that however
 * won't apply for our use case.
 */
class HtmlUtil {

  /**
   * Converts all root-relative URLs to absolute URLs.
   *
   * Does not change any existing protocol-relative or absolute URLs. Does not
   * change other relative URLs because they would result in different absolute
   * URLs depending on the current path. For example: when the same content
   * containing such a relative URL (for example 'image.png'), is served from
   * its canonical URL (for example 'http://example.com/some-article') or from
   * a listing or feed (for example 'http://example.com/all-articles') their
   * "current path" differs, resulting in different absolute URLs:
   * 'http://example.com/some-article/image.png' versus
   * 'http://example.com/all-articles/image.png'. Only one can be correct.
   * Therefore relative URLs that are not root-relative cannot be safely
   * transformed and should generally be avoided.
   *
   * Necessary for HTML that is served outside of a website, for example, RSS
   * and e-mail.
   *
   * @param string $html
   *   The full (X)HTML code to load.
   * @param string $scheme_and_host
   *   The root URL, which has a URI scheme, host and optional port.
   * @param string[] $uri_attributes
   *   All attributes that may contain URIs.
   *
   * @return string
   *   The updated (X)HTML code.
   */
  public static function transformRootRelativeUrlsToAbsolute($html, $scheme_and_host, array $uri_attributes = [
    'href',
    'poster',
    'src',
    'cite',
    'data',
    'action',
    'formaction',
    'srcset',
    'about',
  ]) {
    assert('empty(array_diff(array_keys(parse_url($scheme_and_host)), ["scheme", "host", "port"]))', '$scheme_and_host contains scheme, host and port at most.');
    assert('isset(parse_url($scheme_and_host)["scheme"])', '$scheme_and_host is absolute and hence has a scheme.');
    assert('isset(parse_url($scheme_and_host)["host"])', '$base_url is absolute and hence has a host.');

    $html_dom = new \DOMDocument();
    // Ignore warnings during HTML soup loading.
    @$html_dom->loadHTML($html);
    $xpath = new \DOMXpath($html_dom);

    // Update all root-relative URLs to absolute URLs in the given HTML.
    foreach ($uri_attributes as $attr) {
      foreach ($xpath->query("//*[starts-with(@$attr, '/') and not(starts-with(@$attr, '//'))]") as $node) {
        /** @var \DOMElement $node */
        $node->setAttribute($attr, $scheme_and_host . $node->getAttribute($attr));
      }
      foreach ($xpath->query("//*[@srcset]") as $node) {
        // @see https://html.spec.whatwg.org/multipage/embedded-content.html#attr-img-srcset
        // @see https://html.spec.whatwg.org/multipage/embedded-content.html#image-candidate-string
        $image_candidate_strings = explode(',', $node->getAttribute('srcset'));
        $image_candidate_strings = array_map('trim', $image_candidate_strings);
        for ($i = 0; $i < count($image_candidate_strings); $i++) {
          $image_candidate_string = $image_candidate_strings[$i];
          if ($image_candidate_string[0] === '/' && $image_candidate_string[1] !== '/') {
            $image_candidate_strings[$i] = $scheme_and_host . $image_candidate_string;
          }
        }
        $node->setAttribute('srcset', implode(', ', $image_candidate_strings));
      }
    }
    return HtmlUtil::serialize($html_dom);
  }

  /**
   * Converts the given \DOMDocument back to an HTML string.
   *
   * The resulting (X)HTML code will be properly formatted to be
   * compatible with HTML user agents.
   *
   * @param \DOMDocument $document
   *   A \DOMDocument object to serialize.
   *
   * @return string
   *   A valid (X)HTML code, as a string.
   */
  public static function serialize(\DOMDocument $document) {
    foreach ($document->getElementsByTagName('script') as $node) {
      /** @var \DOMElement $node */
      if ($node->getAttribute('type') == 'application/json') {
        // Do not escape JSON scripts, like the Drupal settings.
        continue;
      }
      Html::escapeCdataElement($node);
    }
    foreach ($document->getElementsByTagName('style') as $node) {
      Html::escapeCdataElement($node, '/*', '*/');
    }
    return $document->saveHTML();
  }

}
