<?php

namespace Drupal\scrapable\Render;

use Drupal\Core\Render\HtmlResponse;

/**
 * The scrapable html response class.
 */
class ScrapableHtmlResponse extends HtmlResponse {
}
