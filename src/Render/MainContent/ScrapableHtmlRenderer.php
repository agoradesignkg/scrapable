<?php

namespace Drupal\scrapable\Render\MainContent;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Render\MainContent\HtmlRenderer;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\scrapable\Render\ScrapableHtmlResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Html renderer implementation for scrapable content.
 */
class ScrapableHtmlRenderer extends HtmlRenderer {

  /**
   * Whether the response should be rendered embedded (without other regions).
   *
   * @var bool
   */
  protected $embedded;

  /**
   * {@inheritdoc}
   *
   * The entire HTML: takes a #type 'page' and wraps it in a #type 'html'.
   */
  public function renderResponse(array $main_content, Request $request, RouteMatchInterface $route_match) {
    list($page, $title) = $this->prepare($main_content, $request, $route_match);

    if (!isset($page['#type']) || $page['#type'] !== 'page') {
      throw new \LogicException('Must be #type page');
    }

    $page['#title'] = $title;

    // Now render the rendered page.html.twig template inside the html.html.twig
    // template, and use the bubbled #attached metadata from $page to ensure we
    // load all attached assets.
    $html = [
      '#type' => 'html',
      'page' => $page,
    ];

    // The special page regions will appear directly in html.html.twig, not in
    // page.html.twig, hence add them here, just before rendering
    // html.html.twig.
    $this->buildPageTopAndBottom($html);

    // Render, but don't replace placeholders yet, because that happens later in
    // the render pipeline. To not replace placeholders yet, we use
    // RendererInterface::render() instead of RendererInterface::renderRoot().
    // @see \Drupal\Core\Render\HtmlResponseAttachmentsProcessor.
    $render_context = new RenderContext();
    $this->renderer->executeInRenderContext($render_context, function () use (&$html) {
      // RendererInterface::render() renders the $html render array and updates
      // it in place. We don't care about the return value (which is just
      // $html['#markup']), but about the resulting render array.
      // @todo Simplify this when https://www.drupal.org/node/2495001 lands.
      $this->renderer->render($html);
    });
    // RendererInterface::render() always causes bubbleable metadata to be
    // stored in the render context, no need to check it conditionally.
    $bubbleable_metadata = $render_context->pop();
    $bubbleable_metadata->applyTo($html);
    $content = $this->renderCache->getCacheableRenderArray($html);

    // Also associate the required cache contexts.
    // (Because we use ::render() above and not ::renderRoot(), we manually must
    // ensure the HTML response varies by the required cache contexts.)
    $content['#cache']['contexts'] = Cache::mergeContexts($content['#cache']['contexts'], $this->rendererConfig['required_cache_contexts']);

    // Also associate the "rendered" cache tag. This allows us to invalidate the
    // entire render cache, regardless of the cache bin.
    $content['#cache']['tags'][] = 'rendered';

    $response = new ScrapableHtmlResponse($content, 200, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ]);

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepare(array $main_content, Request $request, RouteMatchInterface $route_match) {
    $build = parent::prepare($main_content, $request, $route_match);

    if ($this->embedded) {
      $page =& $build[0];

      // Embedded mode is active. We now need to find all regions and remove
      // them from our render array.
      $regions = \Drupal::theme()->getActiveTheme()->getRegions();
      // @todo this must be configurable! (maybe we could also drop this and shift responsiblity into theme layer!
      $keep_regions = ['content', 'help', 'highlighted'];
      foreach ($regions as $region) {
        if (!in_array($region, $keep_regions) && isset($page[$region])) {
          unset($page[$region]);
        }
      }
    }
    return $build;
  }

  /**
   * Sets/unsets the embedded mode (skip rendering of other regions than main).
   *
   * @param bool $embedded
   *   TRUE, if the response should be rendered in embedded mode, FALSE
   *   otherwise. Embedded mode means only rendering the main region, no other.
   */
  public function setEmbedded(bool $embedded) {
    $this->embedded = $embedded;
  }

}
